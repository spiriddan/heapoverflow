from django import forms
from django.contrib.auth.models import User

from django.core.exceptions import ValidationError

from main.models import *


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(min_length=4, widget=forms.PasswordInput)


# profile внутри создаётся
class UserRegisterForm(forms.ModelForm):
    password = forms.CharField(min_length=4, widget=forms.PasswordInput)
    password_check = forms.CharField(min_length=4, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']

    def clean(self):
        password = self.cleaned_data['password']
        password_check = self.cleaned_data['password_check']
        if password != password_check:
            print("asadfsdfsdfsdfsdaf")
            raise ValidationError("Passwords doesn't match")

    def save(self, **kwargs):
        self.cleaned_data.pop('password_check')
        user = User.objects.create_user(**self.cleaned_data)
        profile = Profile(user=user, login=user.username)
        profile.save()
        return user


class SettingForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']
    username = forms.CharField(required=False)
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    email = forms.EmailField(required=False)
    password = forms.CharField(min_length=4, widget=forms.PasswordInput, required=False)
    avatar = forms.ImageField(required=False)

    def save(self, **kwargs):
        user = User.objects.filter(id=kwargs['id'])[0]
        for i in self.cleaned_data:  # чтобы можно было спокойно пустые поля оставлять
            if self.cleaned_data[i] != '':
                user.__dict__[i] = self.cleaned_data[i]

        profile = Profile.objects.filter(user_id=user.id)[0]
        profile.login = user.username
        av = self.cleaned_data.get('avatar')
        if av is not None:
            profile.avatar = av
        profile.save()
        return user.save()


class AskForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['title', 'content', 'tags']

    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all().order_by('title'), widget=forms.CheckboxSelectMultiple)


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['content']

    content = forms.CharField(required=False, label="Answer", widget=forms.Textarea(attrs={'placeholder': "In my opinion..."}))
