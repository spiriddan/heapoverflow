from django.core.management import BaseCommand
from faker import Faker
import random
from main.models import *

fake = Faker()

entities = [User, Profile, Answer, LikeQuestion, LikeAnswer, Question, Tag]


class Command(BaseCommand):  # TODO перейти на другую бд
    help = "Fills database with fake data. Input param num - amount of users"

    def add_arguments(self, parser):
        parser.add_argument("num", type=int)

    def handle(self, *args, **kwargs):
        for e in entities:
            e.objects.all().delete()

        num = kwargs['num']

        users = [User(username=fake.unique.first_name(), email=fake.email()) for _ in range(num)]
        User.objects.bulk_create(users)

        profiles = [
            Profile(
                user=user,
                login=user.username,
            ) for user in users
        ]
        Profile.objects.bulk_create(profiles)

        tags = [Tag(title=fake.text(max_nb_chars=16)) for _ in range(num)]
        Tag.objects.bulk_create(tags)
        tag_ids = list(Tag.objects.values_list('id', flat=True))

        questions = [
            Question(
                title=fake.text(max_nb_chars=64),
                content=fake.text(max_nb_chars=256),
                author=random.choice(profiles),
                creationDate=str(fake.date_between(start_date='-10y', end_date='-1d'))
            ) for _ in range(num*10)
        ]

        Question.objects.bulk_create(questions)
        question_ids = list(Question.objects.values_list('id', flat=True))

        for qid in question_ids:
            tag_to_quest = []
            q_tags = [tag_ids[i] for i in range(0, random.randint(1, len(tags)))]

            for tag in q_tags:
                tag_to_quest.append(Question.tags.through(tag_id=tag, question_id=qid))
            Question.tags.through.objects.bulk_create(tag_to_quest)

        answers = [
            Answer(
                content=fake.text(max_nb_chars=256),
                questions=random.choice(questions),
                author=random.choice(profiles)
            ) for _ in range(num*100)
        ]
        Answer.objects.bulk_create(answers)

        likes = [
            LikeAnswer(
                user=random.choice(profiles),
                answer=random.choice(answers),
                type=random.randint(0, 1)
            ) for _ in range(num * 100)
        ]
        LikeAnswer.objects.bulk_create(likes)

        likes = [
            LikeQuestion(
                user=random.choice(profiles),
                question=random.choice(questions),
                type=random.randint(0, 1)
            ) for _ in range(num * 100)
        ]
        LikeQuestion.objects.bulk_create(likes)

