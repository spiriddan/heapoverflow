from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count


# Create your models here.


class QuestionManager(models.Manager):
    def new(self):  # latest
        return Question.objects.order_by('-creationDate')

    def hot(self):
        return Question.objects.annotate(amount=Count('answer')).order_by('-amount')

    def bytag(self, tag):
        tag = Tag.objects.get(id=tag)
        return tag.questions.all()


class TagsManager(models.Manager):
    def byQuestion(self, question):
        try:
            question = Question.objects.get(id=question)
        except:
            return []
        return question.tags.all()


class ProfileManager(models.Manager):
    def best(self):
        return Profile.objects.order_by('-user_id')[:10]  # TODO добавить лучших


class LikeAnswerManager(models.Manager):
    def answer(self, answer):
        return LikeAnswer.objects.filter(answer=answer)


class LikeQuestionManager(models.Manager):
    def question(self, question):
        return LikeQuestion.objects.filter(question=question)

    def getLikes(self, question):
        return len(LikeQuestion.objects.filter(question=question))

    def toggle_like(self, user, question, type):
        prev = self.filter(user=user, question=question)
        if prev.exists() and prev[0].type == type:
            self.filter(user=user, question=question).delete()
        elif not prev.exists():
            self.create(user=user, question=question, type=type)


class Question(models.Model):
    title = models.CharField(max_length=64)  # 255
    content = models.TextField()
    tags = models.ManyToManyField('Tag', related_name='questions', unique=False)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    creationDate = models.DateTimeField()

    objects = QuestionManager()

class Answer(models.Model):
    content = models.TextField()
    questions = models.ForeignKey('Question', on_delete=models.CASCADE)
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)


class Tag(models.Model):
    title = models.CharField(max_length=16, unique=True)  # TODO type?

    objects = TagsManager()

    def __str__(self):
        return self.title

class Profile(models.Model):
    # username -> nickName; внутри ещё есть password и email
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    login = models.CharField(max_length=64)

    avatar = models.ImageField(null=True, blank=True, default='avatar.jpeg', upload_to="avatar/%Y/%m/%d")

    objects = ProfileManager()


class LikeAnswer(models.Model):
    user = models.ForeignKey('Profile', on_delete=models.CASCADE)  # TODO unique
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    type = models.BooleanField()  # isTYPE

    objects = LikeAnswerManager()


class LikeQuestion(models.Model):
    user = models.ForeignKey('Profile', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    type = models.BooleanField()

    objects = LikeQuestionManager()



