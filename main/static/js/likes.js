function getCookie(name) {
    let cookieValue = null;

    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const items = document.getElementsByClassName('like-section')

for (let item of items){
    const [dislike, counter, like] = item.children
    dislike.addEventListener('click', ()=>{
        const formData = new FormData();
        formData.append('question_id', dislike.dataset.id)

        const request = new Request('/dislike/', {
            method: 'POST',
            body: formData,
            headers:{
                'X-CSRFToken': getCookie("csrftoken")
            }
        })

        fetch(request)
            .then((response) => response.json())
            .then((data) =>{
                counter.innerHTML = data.count;
            });

    })

    like.addEventListener('click', ()=>{
        const formData = new FormData();
        formData.append('question_id', dislike.dataset.id)

        const request = new Request('/like/', {
            method: 'POST',
            body: formData,
            headers:{
                'X-CSRFToken': getCookie("csrftoken")
            }
        })

        fetch(request)
            .then((response) => response.json())
            .then((data) =>{
                counter.innerHTML = data.count;
            });

    })
}