from datetime import datetime
from math import ceil

from django.contrib import auth
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotFound, JsonResponse
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect

from .forms import *
from .models import *

from django.views.decorators.csrf import ensure_csrf_cookie

context = dict()
context['tags'] = Tag.objects.all()
context['bestUsers'] = Profile.objects.best()[:10]

resp404 = "<h1>Станица потерялась в космосе, но вы можете <a href=/> вернуться <a> </h1>"


def paginate(objects, request, per_page=10):
    page = request.GET.get('p', 1)
    page_count = ceil(len(objects) / per_page)  # TODO null pages

    if not (str(page).isdigit()):  # exception от пагинатора
        page = 1

    if str(page).isdigit() and int(page) > page_count:
        page = 1
    paginator = Paginator(objects, per_page)
    return paginator.page(page)


def questionsList(request, questions, per_page=10):
    p = request.GET.get('p', 1)
    page_count = ceil(len(questions) / per_page)

    c = {
        'items': paginate(questions, request, per_page),
        'next': int(int(p) + 1),  # всё из пагинатора можно вытаскивать
        'prev': int(int(p) - 1),
        'max': int(page_count),
        'pages': [i for i in range(1, page_count + 1)],
    }
    return c


def addLikesAndTags(c: dict):
    tmp = list()
    n = 0
    for item in c['items']:
        tmp.append(dict())
        tmp[n]['item'] = item
        tmp[n]['likes'] = len(LikeQuestion.objects.question(item)) if (hasattr(item, 'title')) else len(LikeAnswer.objects.answer(item))
        tmp[n]['tags'] = [t for t in Tag.objects.byQuestion(item.id)][:3]
        n += 1

    c['items'] = tmp


@ensure_csrf_cookie
def landing(request):
    questions = [i for i in Question.objects.new()]
    context.update(questionsList(request, questions))
    addLikesAndTags(context)
    context['is_auth'] = request.user.is_authenticated
    context['answersAmount'] = 1
    return render(request, 'landing.html', context=context)


def hot(request):
    questions = [i for i in Question.objects.hot()]
    context.update(questionsList(request, questions))
    addLikesAndTags(context)
    context['is_auth'] = request.user.is_authenticated
    context['answersAmount'] = 1
    return render(request, 'hot.html', context=context)

@csrf_protect
@login_required(login_url='/login/', redirect_field_name='continue')
def addQuestion(request):
    if request.method == 'GET':
        form = AskForm()
    if request.method == 'POST':
        form = AskForm(request.POST)
        if form.is_valid():
            author = Profile.objects.filter(user_id=request.user.id)[0]
            form.cleaned_data.pop('tags')

            quest = Question(**form.cleaned_data, author=author, creationDate=str(datetime.now()))
            quest.save()

            f2 = AskForm(request.POST, instance=quest)
            f2.save(commit=False)
            f2.save_m2m()
            return redirect(f'/question/{quest.id}')
        else:
            form.add_error(field=None, error="Can't save!")

    context['username'] = request.user.username
    context['form'] = form
    context['is_auth'] = request.user.is_authenticated
    return render(request, 'addQuestion.html', context=context)

@csrf_protect
def question(request, question_id: int):
    if request.method == 'GET':
        form = AnswerForm()
    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            quest = Question.objects.filter(id=question_id)[0]
            author = Profile.objects.filter(user_id=request.user.id)[0]
            ans = Answer(content=form.cleaned_data['content'], questions=quest, author=author)
            if ans is not None:
                ans.save()
            else:
                form.add_error(field=None, error="Can't save!")
    context['form'] = form

    try:
        context['question'] = Question.objects.get(id=question_id)
    except:
        return HttpResponseNotFound(resp404)

    context['questionLikes'] = len(LikeQuestion.objects.question(context['question'])) if (hasattr(context['question'], 'title')) else len(LikeAnswer.objects.answer(context['question']))
    try:
        answers = Answer.objects.filter(questions_id=context['question'].id)
    except:
        answers = []
    context.update(questionsList(request, answers, 3))
    context['answersAmount'] = len(answers)
    addLikesAndTags(context)
    context['is_auth'] = request.user.is_authenticated
    return render(request, 'question.html', context=context)


@csrf_protect
@login_required(login_url='/login/', redirect_field_name='continue')
def settings(request):
    if request.method == 'GET':
        form = SettingForm(request.POST)
    if request.method == 'POST':
        form = SettingForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(id=request.user.id)
        else:
            form.add_error(field=None, error="Can't save!")

    context['username'] = request.user.username
    context['form'] = form
    context['is_auth'] = request.user.is_authenticated
    return render(request, 'settings.html', context=context)


def tags(request, tag):
    try:
        questions = Question.objects.bytag(tag=tag)
    except:
        return HttpResponseNotFound(resp404)
    context.update(questionsList(request, questions))
    addLikesAndTags(context)
    context["tag"] = Tag.objects.get(id=tag).title
    context['is_auth'] = request.user.is_authenticated
    return render(request, 'tags.html', context)

@csrf_protect
def log_in(request):
    if request.method == "GET":
        login_form = LoginForm()

    if request.method == "POST":
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            user = authenticate(request, **login_form.cleaned_data)
            if user is not None:
                login(request, user)
                return redirect(request.GET.get('continue', '/'))
            else:
                login_form.add_error(error="User with this data was not found", field=None)
    context['is_auth'] = request.user.is_authenticated
    context['form'] = login_form
    return render(request, 'login.html', context=context)


def logout(request):
    auth.logout(request)
    return redirect(reverse('login'))

@csrf_protect
def registration(request):
    if request.method == "GET":
        reg_form = UserRegisterForm()

    if request.method == "POST":
        reg_form = UserRegisterForm(request.POST)
        if reg_form.is_valid():
            user = reg_form.save()
            if user is not None:
                login(request, user)
                return redirect(reverse('landing'))
            else:
                reg_form.add_error(error="Not saved", field=None)

    context['is_auth'] = request.user.is_authenticated
    context['form'] = reg_form
    return render(request, 'registration.html', context=context)


@csrf_protect
@login_required(login_url='/login/', redirect_field_name='continue')
def like(request):
    id = request.POST.get('id')
    question = Question.objects.get(pk=id)
    LikeQuestion.objects.toggle_like(user=request.user.profile, question=question, type=True)
    count = LikeQuestion.objects.getLikes(question)

    return JsonResponse({'count': count})

@csrf_protect
@login_required(login_url='/login/', redirect_field_name='continue')
def dislike(request):
    id = request.POST.get('id')
    question = Question.objects.get(pk=id)
    LikeQuestion.objects.toggle_like(user=request.user.profile, question=question, type=False)
    count = LikeQuestion.objects.getLikes(question)

    return JsonResponse({'count': count})
