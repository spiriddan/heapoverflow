"""
URL configuration for tpstackoverflow project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from main import views as v
from tpstackoverflow import settings

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('signup/', v.registration, name='signup'),
    path('ask/', v.addQuestion, name='ask'),
    path('question/<int:question_id>', v.question, name='question'),
    path('tag/<int:tag>', v.tags, name='tag'),
    path('settings/', v.settings, name='settings'),
    path('login/', v.log_in, name='login'),
    path('', v.landing, name='landing'),
    path('hot/', v.hot, name='hot'),
    path('logout/', v.logout, name='logout'),
    path('like/', v.like, name='like'),
    path('dislike/', v.like, name='dislike')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
